# Password Generator

Generate password from salt and key phrase

## Usage

```lang-none
$> password_generator 'salt' 'key'
%"!~g`3boi=,>:.MhLLrD~3s\P=_m^)]
```

\
\
Use

```lang-none
password_generator -h
```

for more information
