use sha3::{Digest, Sha3_256};

pub fn generate(key_phrase: &str, salt: &str) -> String {
    let mut hasher = Sha3_256::new();
    hasher.update(key_phrase.as_bytes());
    hasher.update(salt.as_bytes());
    let result = hasher.finalize();
    u8_to_string(result.as_slice())
}

fn u8_to_string(ns: &[u8]) -> String {
    let mut res = String::new();

    static POSSIBLE_SYMBOLS: &[u8] =
        "1234567890abcdefghigklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~`!@#$%^&*()_-+={[}]|\\:;\"\'<,>.?/".as_bytes();

    for n in ns {
        res.push(POSSIBLE_SYMBOLS[*n as usize % POSSIBLE_SYMBOLS.len()] as char);
    }

    res
}