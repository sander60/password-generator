use std::io::{self, stdin, Write};

use clap::Parser;
use password_generator::generate;

fn main() -> io::Result<()> {
    let cli = Cli::parse();
    let salt = cli
        .salt
        .unwrap_or_else(|| read_string("Input salt: ").unwrap());
    let key_phrase = cli
        .key_phrase
        .unwrap_or_else(|| read_string("Input key phrase: ").unwrap());

    if cli.debug == 1 {
        println!("Salt bytes: {:?}", salt.as_bytes());
        println!("Key phrase bytes: {:?}", key_phrase.as_bytes());
    }

    let password = generate(&key_phrase, &salt);
    println!("{}", password);
    Ok(())
}

#[derive(Parser)]
#[clap(name = "password_generator")]
#[clap(author = "Alexander Chepoi")]
#[clap(version = "1.0.0")]
#[clap(about = "Generate password from salt and key phrase", long_about = None)]
struct Cli {
    #[clap(value_parser)]
    salt: Option<String>,

    #[clap(value_parser)]
    key_phrase: Option<String>,

    #[clap(short, long, action = clap::ArgAction::Count)]
    debug: u8,
}

fn read_string(phrase: &str) -> Result<String, io::Error> {
    print!("{}", phrase);
    io::stdout().flush()?;
    let mut result = String::new();
    stdin().read_line(&mut result)?;
    Ok(String::from(result.trim()))
}
